import React from 'react';
import HomePage from './pages/HomePage';
import ProductPage from './pages/ProductPage';
import AboutPage from './pages/AboutPage';
import EditPage from './pages/EditPage';
import AddPage from './pages/AddPage';
import StatisticsPage from './pages/StatisticsPage';

import { ProductProvider } from './data/ProductContext';


import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';



const App = () => {
  return (
    <ProductProvider>
      <div className="app">
        <Router>
          <Routes>
            <Route path='/' element={<HomePage />} />
            <Route path='/products' element={<HomePage />} />
            <Route path="/product/:productId" element={<ProductPage />} />
            <Route path="/about" element={<AboutPage />} />
            <Route path="/editProduct/:productId" element={<EditPage />} />
            <Route path="/add" element={<AddPage />} />
            <Route path="/statistics" element={<StatisticsPage />} />
          </Routes>
        </Router>

      </div>
    </ProductProvider>
  );
};

export default App;