export const dummyManufacturer = [
  {
    id: '1',
    name: 'Pfizer Inc.'
  },
  {
    id: '2',
    name: 'Novartis International AG'
  },
  {
    id: '3',
    name: 'GlaxoSmithKline plc'
  },
];


export const dummyProducts = [
  {
    id: '1',
    name: 'Aspirin',
    manufacturer: dummyManufacturer[1],
    price: 9.99,
    expiryDate: new Date('2024-12-31'),
  },
  {
    id: '2',
    name: 'Amoxicillin',
    manufacturer: dummyManufacturer[0],
    price: 19.99,
    expiryDate: new Date('2024-12-31'),
  },
  {
    id: '3',
    name: 'Ibuprofen',
    manufacturer: dummyManufacturer[2],
    price: 14.99,
    expiryDate: new Date('2024-12-31'),
  },
  {
    id: '4',
    name: 'Insulin',
    manufacturer: dummyManufacturer[1],
    price: 49.99,
    expiryDate: new Date('2024-12-31'),
  },
  {
    id: '5',
    name: 'Flu Vaccine',
    manufacturer: dummyManufacturer[0],
    price: 29.99,
    expiryDate: new Date('2024-12-31'),
  },
  {
    id: '6',
    name: 'Lisinopril',
    manufacturer: dummyManufacturer[2],
    price: 24.99,
    expiryDate: new Date('2024-12-31'),
  },
  {
    id: '7',
    name: 'Levothyroxine',
    manufacturer: dummyManufacturer[0],
    price: 12.99,
    expiryDate: new Date('2024-12-31'),
  },
  {
    id: '8',
    name: 'Omeprazole',
    manufacturer: dummyManufacturer[1],
    price: 18.99,
    expiryDate: new Date('2024-12-31'),
  },
  {
    id: '9',
    name: 'Salbutamol Inhaler',
    manufacturer: dummyManufacturer[2],
    price: 29.99,
    expiryDate: new Date('2024-12-31'),
  },
  {
    id: '10',
    name: 'Simvastatin',
    manufacturer: dummyManufacturer[0],
    price: 16.99,
    expiryDate: new Date('2024-12-31'),
  },
];