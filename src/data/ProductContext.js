import { createContext, useContext, useState } from 'react';
import { dummyProducts, dummyManufacturer } from '../data/Data';

const ProductContext = createContext();

export const ProductProvider = ({ children }) => {
  const [products, setProducts] = useState(dummyProducts);

  const [manufacturers, setManufacturers] = useState(dummyManufacturer);

  const updateProduct = (updatedProduct) => {
    setProducts((prevProducts) =>
      prevProducts.map((product) =>
        product.id === updatedProduct.id ? updatedProduct : product
      )
    );
  };

  const addProduct = (newProduct) => {
    const foundManufacturer = manufacturers.find(
      (manufacturer) => manufacturer.name === newProduct.manufacturer.name
    );

    if (foundManufacturer) {
      console.log('1')
      newProduct.manufacturer.id = foundManufacturer.id;
    }
    setProducts((prevProducts) => [...prevProducts, newProduct]);
  };

  const deleteProduct = (productId) => {
    setProducts((prevProducts) => prevProducts.filter((product) => product.id !== productId));
  };

  return (
    <ProductContext.Provider value={{ products, updateProduct, addProduct, deleteProduct }}>
      {children}
    </ProductContext.Provider>
  );
};

export const useProductContext = () => {
  return useContext(ProductContext);
};