import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useParams } from 'react-router-dom';
import { useProductContext } from '../data/ProductContext';

import logo from '../pharmacy.png'


const Product = () => {
  const { products, deleteProduct } = useProductContext();
  const { productId } = useParams();

  const navigate = useNavigate();

  const product = products.find((product) => product.id === productId);
  if (!product) {
    return <div>Product not found</div>;
  }

  const handleEdit = () => {
    navigate(`/editProduct/${productId}`);
  };

  const handleDelete = () => {
    const confirmDelete = window.confirm('Are you sure you want to delete this product?');

    if (confirmDelete) {
      deleteProduct(productId);
      navigate('/products');
    }
  };

  return (
    <div className="product-details">
      <form className="product-form">
        <img src={logo} alt={"logo"} className='logo' />
        <h2 className='h2-product'>{product.name}</h2>

        <div className="input-group">
          <label>ID:</label>
          <input type="text" value={product.id} readOnly />
        </div>

        <div className="input-group">
          <label>Manufacturer:</label>
          <input type="text" value={product.manufacturer.name} readOnly />
        </div>

        <div className="input-group">
          <label>Price:</label>
          <input type="text" value={`$${product.price.toFixed(2)}`} readOnly />
        </div>

        <div className="input-group">
          <label>Expiry Date:</label>
          <input type="text" value={formatDate(product.expiryDate)} readOnly />
        </div>

        <div className="button-container">
          <button onClick={handleEdit}>Edit</button>
          <button onClick={handleDelete}>Delete</button>
        </div>
      </form>
    </div>
  );
};

function formatDate(isoDate) {
  const date = new Date(isoDate);
  const day = date.getDate().toString().padStart(2, '0');
  const month = (date.getMonth() + 1).toString().padStart(2, '0');
  const year = date.getFullYear();
  return `${day}.${month}.${year}`;
}

export default Product;