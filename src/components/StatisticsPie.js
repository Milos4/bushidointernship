import React from 'react';
import { Pie } from 'react-chartjs-2';
import { useProductContext } from '../data/ProductContext';
import { Chart, registerables } from 'chart.js';
import { dummyManufacturer } from '../data/Data';


const StatisticsPie = () => {
  const { products } = useProductContext();
  Chart.register(...registerables);

  const productsByManufacturer = dummyManufacturer.reduce((acc, manufacturer) => {
    const products1 = products.filter(product => product.manufacturer.id === manufacturer.id);
    acc[manufacturer.name] = products1;
    return acc;
  }, {});

  const productsCountByManufacturer = Object.values(productsByManufacturer).map(products => products.length);


  const uniqueColors = generateUniqueColors(Object.keys(productsByManufacturer).length);
  const chartData = {
    labels: Object.keys(productsByManufacturer),
    datasets: [
      {
        data: Object.values(productsCountByManufacturer),
        backgroundColor: uniqueColors,
        borderColor: uniqueColors.map(color => color.replace('0.4', '1')),
      },
    ],
  };
  const options = {
  }

  return (
    <div className="statistics-container">
      <div className="chart-container-pie">
        <h3 className='h3-statistics'>Products Distribution by Manufacturer</h3>
        <Pie data={chartData} options={options} />
      </div>
    </div>
  );
};

const generateUniqueColors = (count) => {
  const colors = [];
  for (let i = 0; i < count; i++) {
    const hue = (i * 360) / count;
    colors.push(`hsla(${hue}, 70%, 50%, 0.4)`);
  }
  return colors;
};

export default StatisticsPie;