import React from 'react';
import { useNavigate } from 'react-router-dom';
import { useProductContext } from '../data/ProductContext';

const Home = () => {
  const navigate = useNavigate();

  const { products } = useProductContext();

  const handleRowClick = (productId) => {
    navigate(`/product/${productId}`);
  };
  const handleAdd = () => {
    navigate(`/add`);
  };

  return (
    <div>
      <table className="product-table">
        <thead>
          <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Price</th>
            <th>Expiry Date</th>
          </tr>
        </thead>
        <tbody>
          {products.map((product) => (
            <tr key={product.id} onClick={() => handleRowClick(product.id)}>
              <td>{product.id}</td>
              <td>{product.name}</td>
              <td>{product.manufacturer.name}</td>
              <td>${product.price.toFixed(2)}</td>
              <td>{product.expiryDate.toISOString().split('T')[0]}</td>
            </tr>
          ))}
        </tbody>
      </table>
      <button onClick={handleAdd} className="button-add">Add new product</button>

    </div>
  );
};

export default Home;