import React, { useState } from 'react';
import { useProductContext } from '../data/ProductContext';
import { dummyManufacturer } from '../data/Data';
import { useNavigate } from 'react-router-dom';

import logo from '../pharmacy.png'



const AddProduct = () => {
  const { addProduct, products } = useProductContext();
  const navigate = useNavigate();


  const [productName, setProductName] = useState('');
  const [productPrice, setProductPrice] = useState('');
  const [productExpiryDate, setProductExpiryDate] = useState('');
  const [productManufacturer, setProductManufacturer] = useState('');

  const handleAddProduct = (event) => {
    event.preventDefault();

    if (!productName || !productPrice || !productExpiryDate || !productManufacturer) {
      alert('All fields must be filled out before adding a new product.');
      return;
    }
    const existingIds = products.map(product => parseInt(product.id));
    const maxId = Math.max(...existingIds);


    const newProduct = {

      id: (maxId + 1).toString(),
      name: productName,
      price: parseFloat(productPrice),
      expiryDate: new Date(productExpiryDate),
      manufacturer: { name: productManufacturer },
    };

    addProduct(newProduct);
    navigate(`/products`);

  };

  return (
    <div className="add-product">

      <form className="product-form" onSubmit={handleAddProduct}>
        <img src={logo} alt={"logo"} className='logo' />
        <h3 className='h2-product'>Add new product:</h3>
        <div className="input-group">
          <label>Name:</label>
          <input type="text" value={productName} onChange={(e) => setProductName(e.target.value)} />
        </div>

        <div className="input-group">
          <label>Price:</label>
          <input type="text" value={productPrice} onChange={(e) => setProductPrice(e.target.value)} />
        </div>

        <div className="input-group">
          <label>Expiry Date:</label>
          <input type="date" value={productExpiryDate} onChange={(e) => setProductExpiryDate(e.target.value)} />
        </div>

        <div className="input-group">
          <label>Manufacturer:</label>
          <select value={productManufacturer} onChange={(e) => setProductManufacturer(e.target.value)}>
            {dummyManufacturer.map((manufacturer) => (
              <option key={manufacturer.id} value={manufacturer.name}>
                {manufacturer.name}
              </option>
            ))}
          </select>
        </div>

        <div className="button-container">
          <button type="submit">Add Product</button>
        </div>
      </form>
    </div>
  );
};

export default AddProduct;