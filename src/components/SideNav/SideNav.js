import React, { useState } from 'react';
import './SideNav.css';

import { Link } from 'react-router-dom';
import logo from '../../pharmacy.png'
import { FaBars } from 'react-icons/fa';


function SideNav() {
  return (
    <>
      <div className={`sidenav`}>
        <FaBars className="bars-icon"></FaBars>
        <div className="logo-container">
          <img src={logo} alt={"logo"} className='logo1' />
          <h1>Pharmacy</h1>
        </div>
        <ul>
          <li>
            <Link to="/products" className="nav-link text-white">Products</Link>
          </li>
          <li>
            <Link to="/about" className="nav-link text-white">About</Link>
          </li>
          <li>
            <Link to="/statistics" className="nav-link text-white">Statistics</Link>
          </li>
        </ul>
      </div>
    </>
  );
};

export default SideNav;