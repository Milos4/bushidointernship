import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { dummyManufacturer } from '../data/Data';
import { useNavigate } from 'react-router-dom';

import { useProductContext } from '../data/ProductContext';
import logo from '../pharmacy.png'




const EditProduct = () => {
  const { productId } = useParams();
  const { products } = useProductContext();
  const product = products.find((product) => product.id === productId);

  if (!product) {
    return <div>Product not found</div>;
  }

  const manufacturerNames = dummyManufacturer.map((manufacturer) => manufacturer.name);
  return <EditProductForm product={product} manufacturerNames={manufacturerNames} />;
};

const EditProductForm = ({ product, manufacturerNames }) => {
  const [editedName, setEditedName] = useState('');
  const [editedPrice, setEditedPrice] = useState('');
  const [editedExpiryDate, setEditedExpiryDate] = useState('');
  const [editedManufacturer, setEditedManufacturer] = useState('');

  useEffect(() => {
    setEditedName(product.name);
    setEditedPrice(product.price.toString());
    setEditedExpiryDate(product.expiryDate.toISOString().split('T')[0]);
    setEditedManufacturer(product.manufacturer.name);
  }, [product]);

  const navigate = useNavigate();
  const { updateProduct } = useProductContext();

  const handleSave = () => {
    if (!editedName || !editedPrice || !editedExpiryDate || !editedManufacturer) {
      alert('All fields must be filled out before saving.');
      return;
    }

    const updatedProduct = {
      ...product,
      name: editedName,
      price: parseFloat(editedPrice),
      expiryDate: new Date(editedExpiryDate),
      manufacturer: { name: editedManufacturer },
    };

    updateProduct(updatedProduct);

    navigate(`/product/${product.id}`);
  };

  const handleCancel = () => {
    navigate(`/product/${product.id}`);
  };

  return (
    <div className="product-details">
      <form className="product-form" onSubmit={handleSave}>
        <img src={logo} alt={"logo"} className='logo' />
        <h3 className='h2-product'>Edit product: {product.name}</h3>
        <div className="input-group">
          <label>Name:</label>
          <input type="text" value={editedName} onChange={(e) => setEditedName(e.target.value)} />
        </div>

        <div className="input-group">
          <label>Price:</label>
          <input type="text" value={editedPrice} onChange={(e) => setEditedPrice(e.target.value)} />
        </div>

        <div className="input-group">
          <label>Expiry Date:</label>
          <input type="date" value={editedExpiryDate} onChange={(e) => setEditedExpiryDate(e.target.value)} />
        </div>

        <div className="input-group">
          <label>Manufacturer:</label>
          <select value={editedManufacturer} onChange={(e) => setEditedManufacturer(e.target.value)}>
            {manufacturerNames.map((name) => (
              <option key={name} value={name}>
                {name}
              </option>
            ))}
          </select>
        </div>
        <div className="button-container">
          <button type="submit">Save</button>
          <button onClick={handleCancel}>Cancel</button>
        </div>
      </form>
    </div>
  );
};

export default EditProduct;