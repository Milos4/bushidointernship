import React from 'react';
import logo from '../pharmacy.png'


const About = () => {
  return (
    <>
      <div className='a'>

        <img src={logo} alt={"logo"} className='logo' />
        <h2 className='h2-about'>Pharmacy</h2>

        <h2>About the Application</h2>
        <p>Version: 1.0.0</p>
        <p>Made by: Milos Tomic</p>
      </div>
    </>
  );
};

export default About;