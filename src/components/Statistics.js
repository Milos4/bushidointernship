import React from 'react';
import { Bar } from 'react-chartjs-2';
import { useProductContext } from '../data/ProductContext';
import { Chart, registerables } from 'chart.js';

const Statistics = () => {
  const { products } = useProductContext();
  Chart.register(...registerables);

  const mostExpensiveProducts = products.slice().sort((a, b) => b.price - a.price).slice(0, 5);
  const leastExpensiveProducts = products.slice().sort((a, b) => a.price - b.price).slice(0, 5);

  const chartData = {
    labels: [
      ...mostExpensiveProducts.map(product => product.name),
      ...leastExpensiveProducts.map(product => product.name),
    ],
    datasets: [
      {
        label: 'Most Expensive',
        data: [
          ...mostExpensiveProducts.map(product => product.price),
          ...Array(5).fill(0),
        ],
        backgroundColor: 'rgba(75,192,192,0.4)',
        borderColor: 'rgba(75,192,192,1)',
        borderWidth: 1,
      },
      {
        label: 'Least Expensive',
        data: [
          ...Array(5).fill(0),
          ...leastExpensiveProducts.map(product => product.price),
        ],
        backgroundColor: 'rgba(255,99,132,0.4)',
        borderColor: 'rgba(255,99,132,1)',
        borderWidth: 1,
      },
    ],
  };

  const options = {
    scales: {
      y: {
        beginAtZero: true,
        title: {
          display: true,
          text: 'Price',
        },
      },
    },
  };

  return (
    <div className="statistics-container">
      <h2>Statistics</h2>
      <div className="chart-container">
        <h3 className='h3-statistics'>Top 5 Most Expensive and Least Expensive Products</h3>
        <Bar data={chartData} options={options} />
      </div>
    </div>
  );
};

export default Statistics;