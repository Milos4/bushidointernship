import About from '../components/About';
import Sidebar from '../components/SideNav/SideNav';
import './AboutPage.css';


function HomePage() {
  return (
    <>
      <div>
        <Sidebar />
        <About />
      </div>
    </>
  );
};

export default HomePage;