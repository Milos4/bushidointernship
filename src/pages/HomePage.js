import React, { useState } from 'react';
import './HomePage.css';
import Sidebar from '../components/SideNav/SideNav';
import Home from '../components/Home';



function HomePage() {
  return (
    <>
      <div>
        <Sidebar />
        <Home />
      </div>
    </>
  );
};

export default HomePage;