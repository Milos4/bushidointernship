import React from 'react';
import Sidebar from '../components/SideNav/SideNav';
import Product from '../components/Product';
import './ProductPage.css';



function ProductPage() {
  return (
    <>
      <div>
        <Sidebar />
        <Product />
      </div>
    </>
  );
};

export default ProductPage;