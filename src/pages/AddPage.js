import React from 'react';
import Sidebar from '../components/SideNav/SideNav';
import AddProduct from '../components/AddProduct';
import './ProductPage.css';

function AddPage() {
  return (
    <>
      <div>
        <Sidebar />
        <AddProduct />
      </div>
    </>
  );
};

export default AddPage;