import React from 'react';
import Sidebar from '../components/SideNav/SideNav';
import './StatisticsPage.css';
import Statistics from '../components/Statistics';
import StatisticsPie from '../components/StatisticsPie';



function StatisticsPage() {
    return (
        <div>
            <Sidebar />
            <Statistics />
            <StatisticsPie />
        </div>
    );
};

export default StatisticsPage;