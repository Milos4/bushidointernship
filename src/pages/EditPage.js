import React from 'react';
import Sidebar from '../components/SideNav/SideNav';
import EditProduct from '../components/EditProduct';
import './ProductPage.css';

function EditPage() {
  return (
    <>
      <div>
        <Sidebar />
        <EditProduct />
      </div>
    </>
  );
};

export default EditPage;